#!/bin/bash
set -ex
# Author = Gunjan Sethi gunjan@magikeye.com 
# Version = 1.0

sudo docker build -t mkecc-cluster:1.0 .
sudo docker-compose up -d
sleep 5m
sudo docker exec slurmctld bash -c "/usr/bin/sacctmgr --immediate add cluster name=linux"
sudo docker-compose restart slurmdbd slurmctld
echo 'CLUSTER START UP COMPLETED..'