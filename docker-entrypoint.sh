#!/bin/bash

# Author = Gunjan Sethi gunjan@magikeye.com 
# Version = 1.0

set -e

if [ "$1" = "slurmdbd" ]
then
    echo "---> Starting the RSyslog service ..."
    /etc/init.d/rsyslog start
    echo "---> Starting the MUNGE Authentication service (munged) ..."
    /etc/init.d/munge start

    echo "---> Starting the Slurm Database Daemon (slurmdbd) ..."

    {
        until echo "SELECT 1" | mysql -h mysql -u slurm -ppassword 2>&1 > /dev/null
        do
            echo "-- Waiting for database to become active ..."
            sleep 2
        done
    }
    echo "-- Database is now active ..."

    exec slurmdbd -Dvvv
fi

if [ "$1" = "slurmctld" ]
then
    echo "---> Starting the RSyslog service ..."
    /etc/init.d/rsyslog start
    echo "---> Starting the MUNGE Authentication service (munged) ..."
    /etc/init.d/munge start
    echo "---> Starting the sendmail service ..."
    /etc/init.d/sendmail start

    echo "---> Waiting for slurmdbd to become active before starting slurmctld ..."

    until 2>/dev/null >/dev/tcp/slurmdbd/6819
    do
        echo "-- slurmdbd is not available.  Sleeping ..."
        sleep 2
    done
    echo "-- slurmdbd is now active ..."

    echo "---> Starting the Slurm Controller Daemon (slurmctld) ..."
    exec slurmctld -Dvvv
fi

if [ "$1" = "slurmd" ]
then

    echo "---> Starting the RSyslog service ..."
    /etc/init.d/rsyslog start
    echo "---> Starting the MUNGE Authentication service (munged) ..."
    /etc/init.d/munge start

    until 2>/dev/null >/dev/tcp/slurmctld/6817
    do
        echo "-- slurmctld is not available.  Sleeping ..."
        sleep 2
    done
    echo "-- slurmctld is now active ..."

    echo "---> Starting the Slurm Node Daemon (slurmd) ..."
    exec slurmd -Dvvv
fi

exec "$@"