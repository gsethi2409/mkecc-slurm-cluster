# SLURM Docker Cluster

This is a multi-container SLURM cluster that runs the mkecc library on its compute (or worker) nodes.

## Quickstart

* `git clone git@bitbucket.org:gsethi2409/mkecc-slurm-cluster.git`
* Install docker (https://docs.docker.com/get-docker/)
* Install docker-compose (https://docs.docker.com/compose/install/)
* `./cluster-prune.sh`
* `./start-up.sh`
* `docker exec -it slurmctld bash`
* Run the following to do a quick check!
```console
[root@slurmctld /]# cd /data/
[root@slurmctld data]# sbatch --wrap="uptime"
Submitted batch job 2
[root@slurmctld data]# ls
slurm-2.out
```
## Detailed Cluster Walkthrough
### Containers and Volumes

The compose file will run the following containers:

* mysql
* slurmdbd
* slurmctld
* worker1 (slurmd)
* worker2 (slurmd)
* worker3 (slurmd)
* test2 (slurmd)
* test3 (slurmd)

The compose file will create the following named volumes:

* etc_munge         ( -> /etc/munge     )
* etc_slurm         ( -> /etc/slurm     )
* slurm_jobdir      ( -> /data          )
* var_lib_mysql     ( -> /var/lib/mysql )

### Building the Docker Image

Build the image locally:

```console
sudo docker build -t mkecc-cluster:1.0 .
```

### Starting the Cluster

Run `docker-compose` to instantiate the cluster:

```console
sudo docker-compose up -d
```

### Register the Cluster with SlurmDBD

To register the cluster to the slurmdbd daemon, run the `register_cluster.sh`
script:

```console
sudo docker exec slurmctld bash -c "/usr/bin/sacctmgr --immediate add cluster name=linux"
sudo docker-compose restart slurmdbd slurmctld
```

> Note: You may have to wait 3-5 minutes for the cluster daemons to become
> ready before registering the cluster. Otherwise, you may get an error such
> as **sacctmgr: error: Problem talking to the database: Connection refused**.
>
> You can check the status of the cluster by viewing the logs: `docker-compose
> logs -f`

### Accessing the Cluster

Use `docker exec` to run a bash shell on the controller container:

```console
docker exec -it slurmctld bash
```

From the shell, execute slurm commands, for example:

```console
[root@slurmctld /]# sinfo
PARTITION AVAIL  TIMELIMIT  NODES  STATE NODELIST
Test         up 5-00:00:00      2   idle test[1-2]
Users*       up 5-00:00:00      3   idle worker[1-3]
```

### Submitting Jobs

The `slurm_jobdir` named volume is mounted on each Slurm container as `/data`.
Therefore, in order to see job output files while on the controller, change to
the `/data` directory when on the **slurmctld** container and then submit a job:

```console
[root@slurmctld /]# cd /data/
[root@slurmctld data]# sbatch --wrap="uptime"
Submitted batch job 2
[root@slurmctld data]# ls
slurm-2.out
```

### Checking Job Accounting

```console
sudo docker exec -it slurmdbd bash
root@slurmdbd:/# mysql -h mysql -u slurm -ppassword

MySQL [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| slurm_acct_db      |
+--------------------+

MySQL [(none)]> use slurm_acct_db;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MySQL [slurm_acct_db]> show tables;
+-------------------------------+
| Tables_in_slurm_acct_db       |
+-------------------------------+
| acct_coord_table              |
| acct_table                    |
| clus_res_table                |
| cluster_table                 |
| convert_version_table         |
| federation_table              |
| linux_assoc_table             |
| linux_assoc_usage_day_table   |
| linux_assoc_usage_hour_table  |
| linux_assoc_usage_month_table |
| linux_event_table             |
| linux_job_table               |
| linux_last_ran_table          |
| linux_resv_table              |
| linux_step_table              |
| linux_suspend_table           |
| linux_usage_day_table         |
| linux_usage_hour_table        |
| linux_usage_month_table       |
| linux_wckey_table             |
| linux_wckey_usage_day_table   |
| linux_wckey_usage_hour_table  |
| linux_wckey_usage_month_table |
| qos_table                     |
| res_table                     |
| table_defs_table              |
| tres_table                    |
| txn_table                     |
| user_table                    |
+-------------------------------+

MySQL [slurm_acct_db]> select job_name, id_job from linux_job_table;
+----------+--------+
| job_name | id_job |
+----------+--------+
| wrap     |      2 |
+----------+--------+

```

### Stopping and Restarting the Cluster

```console
docker-compose stop
docker-compose start
```

### Deleting the Cluster

To remove all containers and volumes, run:

```console
docker-compose stop
docker-compose rm -f
docker volume rm $(docker volume ls -q)
```